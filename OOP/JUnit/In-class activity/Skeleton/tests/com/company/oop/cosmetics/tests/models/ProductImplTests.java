package models;

import com.company.oop.cosmetics.exceptions.InvalidUserInputException;
import com.company.oop.cosmetics.models.CategoryImpl;
import com.company.oop.cosmetics.models.GenderType;
import com.company.oop.cosmetics.models.ProductImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;



public class ProductImplTests {
   public static  final int PRODUCT_COUNT = 4;

    public static final String PRODUCT_NAME = "Name";
    public static final String PRODUCT_BRAND = "Brand";
    public static final double PRODUCT_PRICE = 2.5;

    public static final int MAKE_NAME_LEN_MIN = 2;
    public static final int BRAND_NAME_LEN_MIN = 1;
    public static final double PRICE_VAL_MIN = 0;







    public String print() {
        return String.format(
                "#%s %s%n" +
                        " #Price: $%.2f%n" +
                        " #Gender: %s%n",
                PRODUCT_NAME,
                PRODUCT_BRAND,
                PRODUCT_PRICE,
                GenderType.UNISEX);}

    private ProductImpl product;


    @BeforeEach
    public  void setInit(){
        this.product = new ProductImpl(PRODUCT_NAME, PRODUCT_BRAND, PRODUCT_PRICE, GenderType.UNISEX);
    }

    @Test
    public void constructor_Should_InitializeProduct_When_ArgumentsCountMatch() {
        Assertions.assertEquals(PRODUCT_COUNT,4);
    }




    @Test
    public void constructor_Should_ThrowException_When_NameIsShorterThanExpected() {

        Assertions.assertThrows(InvalidUserInputException.class,()-> new ProductImpl("X" ,PRODUCT_BRAND, PRODUCT_PRICE, GenderType.UNISEX));
    }
    @Test
    public void constructor_Should_ThrowException_When_NameIsLongerThanExpected() {
        Assertions.assertThrows(InvalidUserInputException.class,()->new ProductImpl("XXXXXXXXXXX",PRODUCT_BRAND, PRODUCT_PRICE, GenderType.UNISEX));
    }
    @Test
    public void constructor_Should_ThrowException_When_BrandIsShorterThanExpected() {

        Assertions.assertThrows(InvalidUserInputException.class,()-> new ProductImpl(PRODUCT_NAME,"X", PRODUCT_PRICE, GenderType.UNISEX));
    }
    @Test
    public void constructor_Should_ThrowException_When_BrandIsLongerThanExpected() {

        Assertions.assertThrows(InvalidUserInputException.class,()-> new ProductImpl(PRODUCT_NAME,"XXXXXXXXXXXX", PRODUCT_PRICE, GenderType.UNISEX));
    }
    @Test
    public void constructor_Should_ThrowException_When_PriceIsNegative() {

        Assertions.assertThrows(InvalidUserInputException.class,()-> new ProductImpl(PRODUCT_NAME,PRODUCT_BRAND, -1, GenderType.UNISEX));
    }
    @Test
    public  void Should_ThrownException_When_PrintIsNotValid(){

        Assertions.assertEquals(product.print(),print());
    }


}
