package com.company.oop.cosmetics.tests.core;

import org.junit.jupiter.api.Test;

public class ProductRepositoryImplTests {

    @Test
    public void constructor_Should_InitializeProducts() {

    }

    @Test
    public void constructor_Should_InitializeCategories() {

    }

    @Test
    public void getCategories_Should_ReturnCopyOfCollection() {
    }

    @Test
    public void getProducts_Should_ReturnCopyOfCollection() {
    }

    @Test
    public void categoryExists_Should_ReturnTrue_When_CategoryExists() {

    }

    @Test
    public void categoryExists_Should_ReturnFalse_When_CategoryDoesNotExist() {

    }

    @Test
    public void productExists_Should_ReturnTrue_When_ProductExists() {

    }

    @Test
    public void productExists_Should_ReturnFalse_When_ProductDoesNotExist() {

    }

    @Test
    public void createProduct_Should_AddToProducts_When_ArgumentsAreValid() {

    }

    @Test
    public void createCategory_Should_AddToCategories_When_ArgumentsAreValid() {

    }

    @Test
    public void findCategoryByName_Should_ReturnCategory_When_Exists() {

    }

    @Test
    public void findCategoryByName_Should_ThrowException_When_DoesNotExist() {

    }

    @Test
    public void findProductByName_Should_ReturnCategory_When_Exists() {

    }

    @Test
    public void findProductByName_Should_ThrowException_When_DoesNotExist() {

    }

}
