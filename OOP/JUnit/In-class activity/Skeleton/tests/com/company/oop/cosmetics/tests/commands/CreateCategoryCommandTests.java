package commands;

import com.company.oop.cosmetics.commands.CreateCategoryCommand;
import com.company.oop.cosmetics.commands.contracts.Command;
import com.company.oop.cosmetics.core.ProductRepositoryImpl;
import com.company.oop.cosmetics.core.contracts.ProductRepository;
import com.company.oop.cosmetics.exceptions.DuplicateEntityException;
import com.company.oop.cosmetics.exceptions.InvalidUserInputException;
import com.company.oop.cosmetics.models.CategoryImpl;
import com.company.oop.cosmetics.models.contracts.Category;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class CreateCategoryCommandTests {

    private Command createCategory;
    private  ProductRepository productRepository;
    public static final String CATEGORY_NAME = "Name";

    @BeforeEach
    public void beforeEach(){
        productRepository= new ProductRepositoryImpl();
        createCategory =new CreateCategoryCommand(productRepository);

    }


    @Test
    public void execute_Should_AddNewCategoryToRepository_When_ValidParameters() {

        List<String> params = new ArrayList<>();
        params.add(CATEGORY_NAME);
        createCategory.execute(params);
        Category category = this.productRepository.findCategoryByName(CATEGORY_NAME);

        Assertions.assertEquals(CATEGORY_NAME,category.getName());
    }

    @Test
    public void execute_Should_ThrowException_When_MissingParameters() {
        List<String> params = new ArrayList<>();
        Assertions.assertThrows(InvalidUserInputException.class, ()-> createCategory.execute(params));
    }

    @Test
    public void execute_Should_ThrowException_When_DuplicateCategoryName() {
        List<String> params = new ArrayList<>();
        params.add(CATEGORY_NAME);
        this.createCategory.execute(params);
        Assertions.assertThrows(DuplicateEntityException.class, ()-> this.createCategory.execute(params));
    }

}
