package models;

import com.company.oop.cosmetics.exceptions.InvalidUserInputException;
import com.company.oop.cosmetics.models.CategoryImpl;
import com.company.oop.cosmetics.models.GenderType;
import com.company.oop.cosmetics.models.ProductImpl;
import com.company.oop.cosmetics.models.contracts.Product;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class CategoryImplTests {
    private static  final String VALID_NAME =  "xxx";
    public static final int NAME_MIN_LENGTH = 3;
    public static final int NAME_MAX_LENGTH = 10;
    public static final String PRODUCT_NAME = "Name";
    public static final String PRODUCT_BRAND = "Brand";
    public static final double PRODUCT_PRICE = 2.5;
    public static final GenderType PRODUCT_GENDER = GenderType.UNISEX;
    private static final String INVALID_SHORT_NAME = String.valueOf(NAME_MIN_LENGTH-1);
    private static final String PRODUCT_NAME_1 = "Name1";
    private static final String  PRODUCT_BRAND_1 = "Brand1";
    private static final double PRODUCT_PRICE_1 = 5.0;
    private static final String INVALID_LONGER_NAME = String.valueOf(NAME_MAX_LENGTH+1);

    private   CategoryImpl category;
    private ProductImpl products;
    @BeforeEach
    public  void setInit(){
       this.category = new CategoryImpl(VALID_NAME);
        this.products = new ProductImpl(PRODUCT_NAME, PRODUCT_BRAND, PRODUCT_PRICE,GenderType.UNISEX);
    }


    @Test
    public void constructor_Should_InitializeName_When_ArgumentsAreValid() {
        Assertions.assertEquals(category.getName(), VALID_NAME);
    }

    @Test
    public void constructor_Should_InitializeProducts_When_ArgumentsAreValid() {
        ProductImpl product = new ProductImpl(PRODUCT_NAME, PRODUCT_BRAND, PRODUCT_PRICE,PRODUCT_GENDER);
            category.addProduct(product);
            Assertions.assertEquals( category.getProducts().size(),1);
    }

    @Test
    public void constructor_Should_ThrowException_When_NameIsShorterThanExpected() {
            Assertions.assertThrows(InvalidUserInputException.class,()->new CategoryImpl(INVALID_SHORT_NAME));
    }
    @Test
    public void constructor_Should_ThrowException_When_NameIsLongerThanExpected() {
        Assertions.assertThrows(InvalidUserInputException.class,()->new CategoryImpl(INVALID_LONGER_NAME));
    }

    @Test
    public void addProduct_Should_AddProductToList() {

        category.addProduct(products);

        Assertions.assertEquals(1,category.getProducts().size());

    }

    @Test
    public void removeProduct_Should_RemoveProductFromList_When_ProductExist() {
        category.removeProduct(products);
        Assertions.assertEquals(0,category.getProducts().size());
    }

    @Test
    public void removeProduct_should_notRemoveProductFromList_when_productNotExist() {
        category.addProduct(products);
        ProductImpl product1 = new ProductImpl(PRODUCT_NAME_1,PRODUCT_BRAND_1,PRODUCT_PRICE_1,PRODUCT_GENDER);
        category.removeProduct(product1);
        Assertions.assertEquals(1,category.getProducts().size());
    }
    @Test
    public void  print_Should_ReturnCorrectFormat_When_IsEmpty(){
        String output = String.format(
                "#Category: %s%n" +
                        " #No product in this category",
                VALID_NAME);
        Assertions.assertEquals(category.print(),output);
    }
    @Test
    public void  print_Should_ReturnCorrectFormat_When_Called(){
        category.addProduct(products);
        String output1 = String.format("#Category: %s%n" +
                        "#%s %s%n" +
                        " #Price: $%.2f%n" +
                        " #Gender: %s%n" +
                        " ===%n"
                , VALID_NAME
                , PRODUCT_NAME
                , PRODUCT_BRAND
                , PRODUCT_PRICE
                , PRODUCT_GENDER);

        Assertions.assertEquals(category.print(),output1);
    }



}
