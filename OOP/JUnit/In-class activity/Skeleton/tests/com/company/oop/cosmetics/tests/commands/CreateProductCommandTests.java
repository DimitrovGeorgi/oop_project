package com.company.oop.cosmetics.tests.commands;

import com.company.oop.cosmetics.commands.CreateProductCommand;
import com.company.oop.cosmetics.commands.contracts.Command;
import com.company.oop.cosmetics.core.ProductRepositoryImpl;
import com.company.oop.cosmetics.core.contracts.ProductRepository;
import com.company.oop.cosmetics.exceptions.DuplicateEntityException;
import com.company.oop.cosmetics.exceptions.InvalidUserInputException;
import com.company.oop.cosmetics.models.GenderType;
import com.company.oop.cosmetics.models.ProductImpl;
import com.company.oop.cosmetics.models.contracts.Category;
import com.company.oop.cosmetics.models.contracts.Product;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class CreateProductCommandTests {

    private  static String PRODUCT_NAME = "name";
    private  static String PRODUCT_BRAND = "brand";
    private  static double PRODUCT_PRICE = 2.5;

    private  static GenderType PRODUCT_GENDER = GenderType.UNISEX;


    private Product product;
    private  ProductRepository productRepository;
    private Command createProduct;
    private List<String> parameters;



    @BeforeEach
    public void setBefore(){
        productRepository = new ProductRepositoryImpl();
        createProduct = new CreateProductCommand(productRepository);
        product = new ProductImpl(PRODUCT_NAME,PRODUCT_BRAND,PRODUCT_PRICE,PRODUCT_GENDER);
        parameters = new ArrayList<>();
        parameters.add(PRODUCT_NAME);
        parameters.add(PRODUCT_BRAND);
        parameters.add(String.valueOf(PRODUCT_PRICE));
        parameters.add(String.valueOf(PRODUCT_GENDER));
    }


    @Test
    public void execute_Should_AddNewProductToRepository_When_ValidParameters() {

        Assertions.assertEquals(0,productRepository.getProducts().size());
        createProduct.execute(parameters);
        Assertions.assertEquals(1,productRepository.getProducts().size());
    }

    @Test
    public void execute_Should_ThrowException_When_MissingParameters() {
        List<String> params = new ArrayList<>();
        Assertions.assertThrows(InvalidUserInputException.class, ()-> createProduct.execute(params));
    }
    @Test
    public void execute_Should_ThrowException_When_DuplicateProductName() {
        createProduct.execute(parameters);
        Assertions.assertThrows(DuplicateEntityException.class, ()-> this.createProduct.execute(parameters));
    }
    @Test
    public void execute_Should_ThrowException_When_PriceIsNotParsableToDouble() {
        //Adding name as Price in order to test the ParseDouble validation
        this.parameters.set(2, PRODUCT_NAME);
        Assertions.assertThrows(InvalidUserInputException.class, () -> this.createProduct.execute(parameters));
    }
    @Test
    public void execute_Should_ThrowException_When_GenderIsNotParsable() {
        //Adding name as Gender in order to test the ParseGender validation
        this.parameters.set(3, PRODUCT_NAME);
        Assertions.assertThrows(InvalidUserInputException.class, () -> this.createProduct.execute(parameters));
    }

    }
